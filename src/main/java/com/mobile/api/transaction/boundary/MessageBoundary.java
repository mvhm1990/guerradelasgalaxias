package com.mobile.api.transaction.boundary;


import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.ls.LSResourceResolver;

import com.mobile.api.transaction.entity.Avatar;
import com.mobile.api.transaction.entity.characterDetail;
import com.mobile.api.transaction.entity.results;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/message")
@CrossOrigin("*")
public class MessageBoundary {
  

    @GetMapping("/test")
    public Object getMessage() {
        Object obj;
        obj ="miau miau";
        return obj;
    }
    @GetMapping("/avatar")
    public Object getAvatar() {
    Object obj;
    RestTemplate restTemplate = new RestTemplate();
    ResponseEntity<Avatar> avatarResponse=   restTemplate.getForEntity("https://swapi.dev/api/films/", Avatar.class);
    Avatar avatar= avatarResponse.getBody();
    return avatar;
    }
    @GetMapping("/listarxFilm")
    public Object getName(@RequestHeader String filmName)  {
    	 Object obj;
    	
    	    RestTemplate restTemplate = new RestTemplate();
    	    ResponseEntity<Avatar> avatarResponse=   restTemplate.getForEntity("https://swapi.dev/api/films/", Avatar.class);
    	    Avatar avatar= avatarResponse.getBody();
    	if(filmName.equals("4")) {
    	for(int i=0;i<= avatar.getResults().size();i++) {
    		if(avatar.getResults().get(i).getEpisode_id()==4) {
    	
    			return	avatarResponse.getBody().getResults().get(i);
    	
    		}
    	}
    	//	recorrer la lista results
    		//buscar por el episode_id
    	//	si el episode id es 4
    	//	retornas toda la data del 4
    	}
    	if(filmName.equals("5")) {
    	for(int i=0;i<= avatar.getResults().size();i++) {
    		if(avatar.getResults().get(i).getEpisode_id()==5) {
    	return	avatarResponse.getBody().getResults().get(i);
    		}
    	}}
    	if(filmName.equals("6")) {
        	for(int i=0;i<= avatar.getResults().size();i++) {
        		if(avatar.getResults().get(i).getEpisode_id()==6) {
        	return	avatarResponse.getBody().getResults().get(i);
        		}
        	}}
        	if(filmName.equals("3")) {
            	for(int i=0;i<= avatar.getResults().size();i++) {
            		if(avatar.getResults().get(i).getEpisode_id()==3) {
            	return	avatarResponse.getBody().getResults().get(i);
            		}
            	}}
            	if(filmName.equals("2")) {
                	for(int i=0;i<= avatar.getResults().size();i++) {
                		if(avatar.getResults().get(i).getEpisode_id()==2) {
                	return	avatarResponse.getBody().getResults().get(i);
                		}
                	}}
                	if(filmName.equals("1")) {
                    	for(int i=0;i<= avatar.getResults().size();i++) {
                    		if(avatar.getResults().get(i).getEpisode_id()==1) {
							return	 avatarResponse.getBody().getResults().get(i);
                    		}
                    	}
                    	}
    	//	recorrer la lista results
    		//buscar por el episode_id
    	//	si el episode id es 4
    	//	retornas toda la data del 4
    	
    	return	"No se encontro";
    }
  
    @GetMapping("/character-url")
    public Object getUrl(@RequestHeader String requestUrl) {
    	 Object obj;
    requestUrl = "https"+requestUrl.substring(4);
    System.out.println("este es el request Url"+requestUrl.substring(4));
 	    RestTemplate restTemplate = new RestTemplate();
 	    ResponseEntity<characterDetail> avatarResponse=   restTemplate.getForEntity(requestUrl, characterDetail.class);
 	   
 	    characterDetail characterDetail=  avatarResponse.getBody();
 	   
 	
    
		return	characterDetail;
    }

    
}
