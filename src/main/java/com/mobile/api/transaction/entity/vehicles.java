package com.mobile.api.transaction.entity;

import java.util.List;

public class vehicles {
	private List<String> vehicles;

	public List<String> getVehicles() {
		return vehicles;
	}

	public void setVehicles(List<String> vehicles) {
		this.vehicles = vehicles;
	}
}
