package com.mobile.api.transaction.entity;

import java.util.List;

public class starships {
	private List<String> starships;

	public List<String> getStarships() {
		return starships;
	}

	public void setStarships(List<String> starships) {
		this.starships = starships;
	}
}
