package com.mobile.api.transaction.entity;

import java.util.List;

public class Avatar {
private Integer count;
private Integer next;
private Integer previous;
private List<results> results;
public Integer getCount() {
	return count;
}
public void setCount(Integer count) {
	this.count = count;
}
public Integer getNext() {
	return next;
}
public void setNext(Integer next) {
	this.next = next;
}
public Integer getPrevious() {
	return previous;
}
public void setPrevious(Integer previous) {
	this.previous = previous;
}
public List<results> getResults() {
	return results;
}
public void setResults(List<results> results) {
	this.results = results;
}

}
