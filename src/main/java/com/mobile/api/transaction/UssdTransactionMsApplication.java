package com.mobile.api.transaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableDiscoveryClient
public class UssdTransactionMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(UssdTransactionMsApplication.class, args);
    }

}
